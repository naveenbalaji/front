import React ,{useEffect,useState}from 'react'
import Axios from 'axios';
import './main.css';

function Category() {
    const [categoryItems,setCategoryItems]=useState([])

    useEffect( ()=>{
        Axios.get('http://localhost:8000/category').then(res=>{
            setCategoryItems(res.data.data)
    },[])
        
    })
  return(

     <div className="carddecks">    
  {categoryItems.map(item=>(
     <div key={item.imageId} >
     <div className="card cardItem" style={{width:"10rem"}}>
        <img className="card-img-top img-fluid rounded carddecks" src={item.productImagePath}  alt={item.categoryName} />
            <div className="card-body">
                  <p className="card-text">{item.categoryName}</p>
              </div>
        </div> 
 </div>
    ))} 
    </div>   
  );
}

export default Category;

